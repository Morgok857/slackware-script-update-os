#!/bin/bash
version=1.0
manteiner="Pablo Tessarolo<pablo.tessarolo@outlook.com>"

# Set list for a command to  run
command_to_run=(
	'slackpkg update' # Update the slackpkg package database
	'slackpkg install-new' # Install every package which is marked in the Slackware ChangeLog.txt file with the string “Added."
	'slackpkg upgrade-all' # Compare every official Slackware package which you currently have installed, with the package list on your Slackware mirror. If a different version is available, that version will be (downloaded and) upgraded.  
	'slackpkg clean-system' # List will show all packages which has been removed from Slackware
	'lilo' # Make the change permanent and add the new kernel to the lilo boot menu
	'check_available_sbopkg_update' # Update the Slackbuild packet list, validate if there is any update for the Packete 'Sbopkg' and all packages installed with it.
	)

# Simple function to print a custom banner
function print_banner() {
	echo
	echo
	echo "============================================"
	echo "$1"
	echo "============================================"
	echo
	echo
	echo

}

function check_available_sbopkg_update() {
	echo;
	print_banner "Starting Update all packages from SlackBuilds";
	echo;
	sbopkg -r; # ``Rsync'' the local repository with SBo and quit
	echo;
	sbopkg -u; # Check for an update to sbopkg itself and then quit
	echo;
	sbopkg -c; # Display a list of installed SBo packages and potential updates
	echo;
	echo "Creating a list a packages available to be updated automatically . . .";
	getListPotentialUpdate=$(sbopkg -c|grep -B1 "POTENTIAL UPDATE"|grep -v -e "POTENTIAL UPDATE" -e "--"|sed 's/://g');
	echo;
	echo "Installing updates . . .";
	sbopkg -b " $getListPotentialUpdate ";
	echo;
	return 0;
}

# Starting main 
print_banner "Starting Updating Slackware"

# This loop goes througth the list of commands and runs them one at a time.
for comm in "${command_to_run[@]}";
do
	print_banner "Command: $comm"
	$comm
done

echo
echo
echo "[*] Slackware Based updated."
echo
echo

