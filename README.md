# SOSUpgrade (Slackware Script Update OS)

# Description

This is my personal script to update my Operating System. I call it 'SOSUpgrade', the first 'S' is there because it's for Slackware.

**NOTE:* This script requires human interaction to work during its execution.

# Requeriment

* Slackware OS
* User with permission to execute system command
* (Optional) Installed sbopkg [https://sbopkg.org/](https://sbopkg.org/)

# Instalation

First, clone repo with: 

```
$ git clone git@gitlab.com:Morgok857/sosupgrade.git
```

Go to set permissions:

```
$ cd sosupgrade
```

You have 2 files. If you run a script with 'sbopkg' checks use 'sosupgrade-no-sbopkg.sh'. For only base system use 'sosupgrade.sh'.

Give an execution permission:

```
$ chmod +x sosupgrade.sh
```

Now, run script:

```
# bash ./sosupgrade.sh
```

# Customization

If yo need change, delte or add news commands. You need edit file and apply this change into array named 'command_to_run' (Line 6)

For example, I like print the Slackware version beafore all commands ussing 'cat /etc/slackware-version'. 

Beafore array:

```
command_to_run=(
	'slackpkg update' # Update the slackpkg package database
	'slackpkg install-new' # Install every package which is marked in the Slackware ChangeLog.txt file with the string “Added."
	'slackpkg upgrade-all' # Compare every official Slackware package which you currently have installed, with the package list on your Slackware mirror. If a different version is available, that version will be (downloaded and) upgraded.  
	'slackpkg clean-system' # List will show all packages which has been removed from Slackware
	'lilo' # Make the change permanent and add the new kernel to the lilo boot menu
	'sbopkg -r' # ``Rsync'' the local repository with SBo and quit
	'sbopkg -u' # Check for an update to sbopkg itself and then quit
	'sbopkg -c' # Display a list of installed SBo packages and potential updates
	)
```

Now array:

```
command_to_run=(
    'cat /etc/slackware-version'
	'slackpkg update' # Update the slackpkg package database
	'slackpkg install-new' # Install every package which is marked in the Slackware ChangeLog.txt file with the string “Added."
	'slackpkg upgrade-all' # Compare every official Slackware package which you currently have installed, with the package list on your Slackware mirror. If a different version is available, that version will be (downloaded and) upgraded.  
	'slackpkg clean-system' # List will show all packages which has been removed from Slackware
	'lilo' # Make the change permanent and add the new kernel to the lilo boot menu
	'sbopkg -r' # ``Rsync'' the local repository with SBo and quit
	'sbopkg -u' # Check for an update to sbopkg itself and then quit
	'sbopkg -c' # Display a list of installed SBo packages and potential updates
	)
```

**Note:** You can, or not, add comment next to command for any explanation same as other commands.
